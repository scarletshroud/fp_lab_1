-module (prime_lists).
-export ([start/0]).
-import (math, [sqrt/1]).

generate_list(List, I, Limit) when I >= Limit -> lists:reverse(List);
generate_list(List, I, Limit) when I < Limit -> generate_list([I | List], I + 2, Limit).

is_prime(Number, Counter) ->
    if 
        (Number rem Counter == 0) -> false;
        (Number rem Counter /= 0) -> 
            Limit = sqrt(Number),
            if 
                (Counter < Limit) -> is_prime(Number, Counter + 1);
                (Counter >= Limit) -> true
            end
    end.

start() ->
    Numbers = generate_list([], 1, 150000),
    PrimeNumbers = lists:filter(fun(T) -> is_prime(T, 2) end, Numbers),
    X = lists:nth(10001, PrimeNumbers),
    io:fwrite("~w\n", [X]).