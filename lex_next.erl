-module(lex_next).
-export([nextPermutation/1, start/0]).

swap(List, S1, S2) -> 
    {List2, [F | List3]} = lists:split(S1 - 1, List),
    LT = List2++[lists:nth(S2, List) | List3],
    {List4, [_ | List5]} = lists:split(S2 - 1, LT),
    List4++[F | List5].

findSuffix(Numbers, I, PrevElement) ->
    CurrentElement = lists:nth(I, Numbers),
    if 
        (I == 1) -> I + 1;
        (PrevElement > CurrentElement) -> I;
        (PrevElement =< CurrentElement) -> findSuffix(Numbers, I - 1, CurrentElement)
    end.

findMin(Numbers, I, Max) ->
    CurrentElement = lists:nth(I, Numbers), 
    if
        (I == 1) -> I;
        (Max < CurrentElement) -> I;
        (Max >= CurrentElement) -> findMin(Numbers, I - 1, Max)
    end. 

nextPermutation(Numbers) ->
    Suffix = findSuffix(Numbers, 10, lists:nth(10, Numbers)),
    Min = findMin(Numbers, 10, lists:nth(Suffix, Numbers)),
    NewList = swap(Numbers, Suffix, Min),
    {FirstPart, SecondPart} = lists:split(Suffix, NewList),
    lists:append(FirstPart, lists:sort(SecondPart)).

findPermutation(Numbers, I, Limit) ->
    if 
        (I == Limit) -> Numbers;
        (I < Limit) -> 
            NextPermutation = nextPermutation(Numbers),
            findPermutation(NextPermutation, I + 1, Limit)
    end.

start() ->
    Numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    Result = findPermutation(Numbers, 1, 1000000),
    io:fwrite("~w~n", [Result]).